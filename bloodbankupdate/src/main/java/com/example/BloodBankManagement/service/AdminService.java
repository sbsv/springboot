package com.example.BloodBankManagement.service;

import com.example.BloodBankManagement.model.Admin;

public interface AdminService {
	public Admin adminAuthentication(Admin admin);
}
