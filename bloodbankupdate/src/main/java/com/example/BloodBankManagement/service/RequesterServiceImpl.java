package com.example.BloodBankManagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BloodBankManagement.dao.RequesterRepository;
import com.example.BloodBankManagement.model.Requester;

@Service
public class RequesterServiceImpl implements RequesterService{
	
	@Autowired
	RequesterRepository requesterRepository;

	@Override
	public Requester requesterRegistration(Requester requester) {
		return requesterRepository
				.save(new Requester(requester.getPatientName(),requester.getRequiredBloodGroup(),requester.getCity(),requester.getDoctorName(),requester.getHospitalName(),requester.getHospitalAddress(),requester.getRequestedDate(),requester.getContactName(),requester.getContactNumber(),requester.getContactEmailId(),requester.getMessage()));
	}

	@Override
	public Requester requesterStatus(long id) {
		Optional<Requester> optional= requesterRepository.findById(id);
		Requester databaseRequester= null;
		if(optional.isPresent()) {
			databaseRequester = optional.get();
			return databaseRequester;
		}
		return null;
	}

	@Override
	public void requesterStatusUpdate(Long requesterId, String status) {
		requesterRepository.updateRequesterStatus(status, requesterId);
	}

	@Override
	public List<Requester> getRequestersForApproval() {
		return requesterRepository.findAllRequestersForApproval();
	}

}
