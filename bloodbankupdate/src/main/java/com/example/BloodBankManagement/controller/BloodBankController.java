package com.example.BloodBankManagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BloodBankManagement.model.Admin;
import com.example.BloodBankManagement.model.Donor;
import com.example.BloodBankManagement.model.Requester;
import com.example.BloodBankManagement.service.AdminService;
import com.example.BloodBankManagement.service.DonorService;
import com.example.BloodBankManagement.service.RequesterService;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class BloodBankController {
	
	@Autowired
	DonorService donorService; 
	
	@Autowired
	RequesterService requesterService; 
	
	@Autowired
	AdminService adminService;
	
	@PostMapping(value = "/donor")
	public ResponseEntity<Donor> postDonor(@RequestBody Donor donor) {
		try {
			return new ResponseEntity<>(donorService.donorRegistration(donor), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@PostMapping(value = "/requester")
	public ResponseEntity<Requester> postRequester(@RequestBody Requester requester) {
		try {
			return new ResponseEntity<>(requesterService.requesterRegistration(requester), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@PostMapping(value = "/admin")
	public ResponseEntity<Admin> adminAutentication(@RequestBody Admin admin) {
		try {
			return new ResponseEntity<>(adminService.adminAuthentication(admin), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping(value = "donors/bloodGroup/{bloodGroup}")
	  public ResponseEntity<List<Donor>> findDonarByBloodGroup(@PathVariable String bloodGroup) {
	    try {
	      List<Donor> donors = donorService.findDonarByBloodGroup(bloodGroup);

	      if (donors.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }
	      return new ResponseEntity<>(donors, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }
	
	@GetMapping("/donors/{id}")
	public ResponseEntity<Donor> getDonorsById(@PathVariable("id") long id) {
		try {
			return new ResponseEntity<>(donorService.donorStatus(id), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/requesters/{id}")
	public ResponseEntity<Requester> getRequestersById(@PathVariable("id") long id) {
		try {
			return new ResponseEntity<>(requesterService.requesterStatus(id), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/donors")
	public ResponseEntity<List<Donor>> getDonorsForApproval() {
		try {
			return new ResponseEntity<>(donorService.getDonorsForApproval(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/donors/{donorId}/{status}")
	public void donorStatusUpdate(@PathVariable("donorId") long donorId,@PathVariable("status") String status) {
		try {
			donorService.donorStatusUpdate(donorId, status);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	@GetMapping("/requesters")
	public ResponseEntity<List<Requester>> getRequestersForApproval() {
		try {
			return new ResponseEntity<>(requesterService.getRequestersForApproval(), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@GetMapping("/requesters/{requesterId}/{status}")
	public void requesterStatusUpdate(@PathVariable("requesterId") long requesterId,@PathVariable("status") String status) {
		try {
			requesterService.requesterStatusUpdate(requesterId, status);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
}
