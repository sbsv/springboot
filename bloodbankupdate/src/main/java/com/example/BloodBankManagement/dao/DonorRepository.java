package com.example.BloodBankManagement.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.BloodBankManagement.model.Donor;

public interface DonorRepository extends CrudRepository<Donor, Long>{
	List<Donor> findByBloodGroup(String bloodGroup);
	@Query(value="select * from donor where status = 'pending'", nativeQuery=true)
	public List<Donor> findAllDonorsForApproval();
	@Transactional
	@Modifying
	@Query("update Donor d set d.status = :status where d.donorId = :donorId")
	int updateDonorStatus(@Param("status") String status, @Param("donorId") Long donorId);
}
