import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Requester } from '../Requester';
import { RequesterserviceService } from '../requesterservice.service';

@Component({
  selector: 'app-requesterapproval',
  templateUrl: './requesterapproval.component.html',
  styleUrls: ['./requesterapproval.component.css']
})
export class RequesterapprovalComponent implements OnInit {
  requesters: Observable<Requester[]>;
  constructor(private requesterService:RequesterserviceService) { }

  ngOnInit(): void {
    this.reloadData();
  }
  reloadData(){
    this.requesters = this.requesterService.getRequestersForApproval();
  }
}
