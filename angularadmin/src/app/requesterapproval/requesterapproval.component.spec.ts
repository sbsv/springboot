import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequesterapprovalComponent } from './requesterapproval.component';

describe('RequesterapprovalComponent', () => {
  let component: RequesterapprovalComponent;
  let fixture: ComponentFixture<RequesterapprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequesterapprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequesterapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
