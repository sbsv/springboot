import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequesterregistrationComponent } from './requesterregistration.component';

describe('RequesterregistrationComponent', () => {
  let component: RequesterregistrationComponent;
  let fixture: ComponentFixture<RequesterregistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequesterregistrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequesterregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
