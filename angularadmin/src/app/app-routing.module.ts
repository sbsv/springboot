import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { DonorComponent } from './donor/donor.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { DonorapprovalComponent } from './donorapproval/donorapproval.component';
import { RequesterapprovalComponent } from './requesterapproval/requesterapproval.component';
import { RequesterComponent } from './requester/requester.component';
import { DonorregistrationComponent } from './donorregistration/donorregistration.component';
import { RequesterregistrationComponent } from './requesterregistration/requesterregistration.component';

const routes: Routes = [
  {path:"admin", component:AdminComponent},
  {path:"donor", component:DonorComponent},
  {path:"requester", component:RequesterComponent},
  {path:"dreg",component:DonorregistrationComponent},
  {path:"rreg",component:RequesterregistrationComponent},
  {path:"adminLogin",component:AdminloginComponent},
  {path:"donorApproval",component:DonorapprovalComponent},
  {path:"requesterApproval",component:RequesterapprovalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
