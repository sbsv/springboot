import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonorapprovalComponent } from './donorapproval.component';

describe('DonorapprovalComponent', () => {
  let component: DonorapprovalComponent;
  let fixture: ComponentFixture<DonorapprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonorapprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonorapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
