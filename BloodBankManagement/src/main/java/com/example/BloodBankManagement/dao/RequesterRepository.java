package com.example.BloodBankManagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.BloodBankManagement.model.Requester;

public interface RequesterRepository extends CrudRepository<Requester, Long>{

}
