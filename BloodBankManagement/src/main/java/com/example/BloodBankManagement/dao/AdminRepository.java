package com.example.BloodBankManagement.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.BloodBankManagement.model.Admin;

public interface AdminRepository extends CrudRepository<Admin, String>{
}
