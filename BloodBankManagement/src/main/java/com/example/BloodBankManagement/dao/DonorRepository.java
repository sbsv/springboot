package com.example.BloodBankManagement.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.BloodBankManagement.model.Donor;

public interface DonorRepository extends CrudRepository<Donor, Long>{
	List<Donor> findByBloodGroup(String bloodGroup);
}
