package com.example.BloodBankManagement.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.BloodBankManagement.dao.DonorRepository;
import com.example.BloodBankManagement.model.Donor;

@Service
public class DonorServiceImpl implements DonorService{
	
	@Autowired
	DonorRepository donorRepository; 

	@Override
	public Donor donorRegistration(Donor donor) {
		return donorRepository
		.save(new Donor(donor.getFirstName(),donor.getLastName(),donor.getCity(),donor.getBloodGroup(),donor.getTimeOfTheDay(),donor.getBloodGlucoseLevel(),donor.getNotes()));
	}

	@Override
	public List<Donor> findDonarByBloodGroup(String bloodGroup) {
		return donorRepository.findByBloodGroup(bloodGroup);
	}

	@Override
	public Donor donorStatus(long id) {
		Optional<Donor> optional= donorRepository.findById(id);
		Donor databaseDonor= null;
		if(optional.isPresent()) {
			databaseDonor = optional.get();
			return databaseDonor;
		}
		return null;
	}

}
