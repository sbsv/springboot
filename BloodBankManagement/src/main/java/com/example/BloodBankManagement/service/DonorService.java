package com.example.BloodBankManagement.service;

import java.util.List;

import com.example.BloodBankManagement.model.Donor;

public interface DonorService {
	public Donor donorRegistration(Donor donor);
	public List<Donor> findDonarByBloodGroup(String bloodGroup);
	public Donor donorStatus(long id);
}
