package com.example.BloodBankManagement.service;

import com.example.BloodBankManagement.model.Requester;

public interface RequesterService {
	Requester requesterRegistration(Requester requester);
	Requester requesterStatus(long id);
}
