package com.example.BloodBankManagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="donor")
public class Donor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long donorId;
	private String firstName;
	private String lastName;
	private String city;
	private String bloodGroup;
	private Date timeOfTheDay;
	private int bloodGlucoseLevel;
	private String notes;
	@Column(columnDefinition = "varchar(15) default 'pending'")
	private String status = "pending";
	public Donor() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Donor(String firstName, String lastName, String city, String bloodGroup, Date timeOfTheDay,
			int bloodGlucoseLevel, String notes) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.bloodGroup = bloodGroup;
		this.timeOfTheDay = timeOfTheDay;
		this.bloodGlucoseLevel = bloodGlucoseLevel;
		this.notes = notes;
	}
	public Donor(long donorId, String firstName, String lastName, String city, String bloodGroup, Date timeOfTheDay,
			int bloodGlucoseLevel, String notes, String status) {
		super();
		this.donorId = donorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.bloodGroup = bloodGroup;
		this.timeOfTheDay = timeOfTheDay;
		this.bloodGlucoseLevel = bloodGlucoseLevel;
		this.notes = notes;
		this.status = status;
	}
	public long getDonorId() {
		return donorId;
	}
	public void setDonorId(long donorId) {
		this.donorId = donorId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public Date getTimeOfTheDay() {
		return timeOfTheDay;
	}
	public void setTimeOfTheDay(Date timeOfTheDay) {
		this.timeOfTheDay = timeOfTheDay;
	}
	public int getBloodGlucoseLevel() {
		return bloodGlucoseLevel;
	}
	public void setBloodGlucoseLevel(int bloodGlucoseLevel) {
		this.bloodGlucoseLevel = bloodGlucoseLevel;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Donor [donorId=" + donorId + ", firstName=" + firstName + ", lastName=" + lastName + ", city=" + city
				+ ", bloodGroup=" + bloodGroup + ", timeOfTheDay=" + timeOfTheDay + ", bloodGlucoseLevel="
				+ bloodGlucoseLevel + ", notes=" + notes + ", status=" + status + "]";
	}
}
