package com.example.BloodBankManagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="requester")
public class Requester {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long requesterId;
	private String patientName;
	private String requiredBloodGroup;
	private String city;
	private String doctorName;
	private String hospitalName;
	private String hospitalAddress;
	private Date requestedDate;
	private String contactName;
	private String contactNumber;
	private String contactEmailId;
	private String message;
	@Column(columnDefinition = "varchar(15) default 'pending'")
	private String status="pending";
	public Requester() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Requester(String patientName, String requiredBloodGroup, String city, String doctorName, String hospitalName,
			String hospitalAddress, Date requestedDate, String contactName, String contactNumber, String contactEmailId,
			String message) {
		super();
		this.patientName = patientName;
		this.requiredBloodGroup = requiredBloodGroup;
		this.city = city;
		this.doctorName = doctorName;
		this.hospitalName = hospitalName;
		this.hospitalAddress = hospitalAddress;
		this.requestedDate = requestedDate;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.contactEmailId = contactEmailId;
		this.message = message;
	}
	public Requester(long requesterId, String patientName, String requiredBloodGroup, String city, String doctorName,
			String hospitalName, String hospitalAddress, Date requestedDate, String contactName, String contactNumber,
			String contactEmailId, String message, String status) {
		super();
		this.requesterId = requesterId;
		this.patientName = patientName;
		this.requiredBloodGroup = requiredBloodGroup;
		this.city = city;
		this.doctorName = doctorName;
		this.hospitalName = hospitalName;
		this.hospitalAddress = hospitalAddress;
		this.requestedDate = requestedDate;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.contactEmailId = contactEmailId;
		this.message = message;
		this.status = status;
	}
	public long getRequesterId() {
		return requesterId;
	}
	public void setRequesterId(long requesterId) {
		this.requesterId = requesterId;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getRequiredBloodGroup() {
		return requiredBloodGroup;
	}
	public void setRequiredBloodGroup(String requiredBloodGroup) {
		this.requiredBloodGroup = requiredBloodGroup;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalAddress() {
		return hospitalAddress;
	}
	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactEmailId() {
		return contactEmailId;
	}
	public void setContactEmailId(String contactEmailId) {
		this.contactEmailId = contactEmailId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Requester [requesterId=" + requesterId + ", patientName=" + patientName + ", requiredBloodGroup="
				+ requiredBloodGroup + ", city=" + city + ", doctorName=" + doctorName + ", hospitalName="
				+ hospitalName + ", hospitalAddress=" + hospitalAddress + ", requestedDate=" + requestedDate
				+ ", contactName=" + contactName + ", contactNumber=" + contactNumber + ", contactEmailId="
				+ contactEmailId + ", message=" + message + ", status=" + status + "]";
	}
}
