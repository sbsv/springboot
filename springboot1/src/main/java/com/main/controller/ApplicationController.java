package com.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.main.model.Login;

@RestController
public class ApplicationController {
	
	@GetMapping("/login")
	public ModelAndView loadLogin(@ModelAttribute("login") Login login) {
		return new ModelAndView("login");
	}

	//getmapping
	@GetMapping("/register")
	public ModelAndView loadRegister() {
		//postmapping
		//int a=10;
		return new ModelAndView("Register");
	}
}
