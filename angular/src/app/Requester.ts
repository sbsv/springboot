export class Requester{
    requesterId:number;
	patientName:string;
	requiredBloodGroup:string;
	city:string;
	doctorName:string;
	hospitalName:string;
	hospitalAddress:string;
	requestedDate:Date;
	contactName:string;
	contactNumber:string;
	contactEmailId:string;
	message:string;
	status:string;
}