import { Component, OnInit } from '@angular/core';
import { Requester } from '../Requester';
import { RequesterserviceService } from '../requesterservice.service';
import { Donor } from '../Donor';

@Component({
  selector: 'app-requester',
  templateUrl: './requester.component.html',
  styleUrls: ['./requester.component.css']
})
export class RequesterComponent implements OnInit {
  requesterId:number;
  bloodGroup:string;
  donors: Donor[];
  requester:Requester;

  constructor(private requesterService:RequesterserviceService) { }

  ngOnInit(): void {
    this.requesterId = 0;
    this.bloodGroup = "bloodgroup";
  }
  
  private getRequesterStatus() {
    this.requesterService.getRequesterById(this.requesterId)
      .subscribe(requester => this.requester = requester);
  }

  private getDonors(){
    this.donors = [];
    this.requesterService.getDonorsByBloodGroup(this.bloodGroup)
      .subscribe(donors => this.donors = donors);
  }

  onSubmit() {
    this.getRequesterStatus();
  }

  onSubmitBloodGroup(){
    this.getDonors();
  }
}
