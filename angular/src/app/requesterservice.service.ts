import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequesterserviceService {
  private baseUrl = 'http://localhost:8080/api/requester';

  constructor(private http:HttpClient) { }

  requesterRegistration(requester:any):Observable<any>{
    return this.http.post(this.baseUrl,requester);
  }

  private dUrl = 'http://localhost:8080/api/requesters';
  getRequesterById(requesterId:number):Observable<any>{
    return this.http.get(`${this.dUrl}/${requesterId}`);
  }

  private bgUrl = 'http://localhost:8080/api/donors';
  getDonorsByBloodGroup(bloodGroup:string):Observable<any>{
    return this.http.get(`${this.bgUrl}/bloodGroup/${bloodGroup}`);
  }
}
