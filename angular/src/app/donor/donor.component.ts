import { Component, OnInit } from '@angular/core';
import { DonorserviceService } from '../donorservice.service';
import { Donor } from '../Donor';

@Component({
  selector: 'app-donor',
  templateUrl: './donor.component.html',
  styleUrls: ['./donor.component.css']
})
export class DonorComponent implements OnInit {

  donorId:number;
  donor:Donor;

  constructor(private donorService:DonorserviceService) { }

  ngOnInit(): void {
    this.donorId = 0;
  }
  
  private getDonorStatus() {
    this.donorService.getDonorById(this.donorId)
      .subscribe(donor => this.donor = donor);
  }

  onSubmit() {
    this.getDonorStatus();
  }
}
