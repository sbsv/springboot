import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DonorComponent } from './donor/donor.component';
import { RequesterComponent } from './requester/requester.component';
import { AdminComponent } from './admin/admin.component';
import { DonorregistrationComponent } from './donorregistration/donorregistration.component';
import { HttpClientModule } from '@angular/common/http';
import { RequesterregistrationComponent } from './requesterregistration/requesterregistration.component';

@NgModule({
  declarations: [
    AppComponent,
    DonorComponent,
    RequesterComponent,
    AdminComponent,
    DonorregistrationComponent,
    RequesterregistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
