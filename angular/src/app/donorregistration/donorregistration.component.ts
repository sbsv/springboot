import { Component, OnInit } from '@angular/core';
import { DonorserviceService } from '../donorservice.service';
import { Donor } from '../Donor';

@Component({
  selector: 'app-donorregistration',
  templateUrl: './donorregistration.component.html',
  styleUrls: ['./donorregistration.component.css']
})
export class DonorregistrationComponent implements OnInit {

  donor:Donor = new Donor();
  submitted:boolean = false;

  ngOnInit(): void {
  }

  constructor(private donorService:DonorserviceService) { }
  save(){
    this.donorService.donorRegistration(this.donor)
    .subscribe(
      data => {
        console.log(data);
        this.submitted = true;
      },
      error => console.log(error)
    );
    this.donor = new Donor();
  }
  onSubmit(){
    this.save();
  }
}
